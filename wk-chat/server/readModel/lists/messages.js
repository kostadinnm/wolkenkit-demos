const fields = {
    text: { initialState: "" },
    likes: { intialState: 0 },
    //fastLookup engages indexing on the field in the db
    timestamp: { intialState: 0, fastLookup: true }
};
const projections = {
    //todo: think over syntax
    "communication.message.sent": function(messages, event) {
        messages.add({
            text: event.data.text,
            timestamp: event.metadata.timestamp
        });
    },
    // "communication.message.liked": function(messages, event) {
    //     messages.update({
    //         where: {id: event.aggregate.id},
    //         set: { likes: event.data.likes}
    //     });
    // }
    async "communication.message.liked"(messages, event) {
        messages.update({
            where: { id: event.aggregate.id },
            set: { likes: event.data.likes }
        });
    }
};

module.exports = { fields, projections };
