const fields = {
    key: { initialState: "" },
    value: { initialState: 0 }
};
const projections = {
    "playing.game.succeeded": function(statistics, event) {
        statistics.update({
            where: { key: "highscore", value: { $lessThan: event.data.level } },
            set: { value: event.data.level }
        });
    }
};

module.exports = { fields, projections };
